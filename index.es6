// HTML DATA //
const readme = require("../../README.md");
const md = require.context("../../docs", true, /\.md$/);
const html = require.context("../../docs", true, /\.html$/);
const ejs = require.context("raw!../../docs", true, /\.ejs$/);

module.exports = {
  read: function( location ){
      let content = "";
      try {
        if(location == '/readme.md'){
          content = readme;
      } else if (location && location.match(/\.md$/)) {
          let code = "";
          content = md('./' + location);
        } else if (location && location.match(/\.html$/)) {
          let code = "";
          content = html('./' + location);
        } else if( location && location.match(/\.ejs$/) ){
          let code = "";
          content = ejs('./' + location);

        } else {
          content = "";
        }
      } catch (e) {
        console.log("render error while loading %s", ('./' + location), e);
      }
      return content;
    }
}
